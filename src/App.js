import React, {Fragment, Component} from 'react';
import './App.css';
import './spinner.css';
import {Total, DevisID, Company, Client, Logo, Table, Payement, Signature, Line, LineCenter} from './devis/components';
import Devis from './devis/parser';

const SwitchButton = ({hook}) => (
  <button 
    style={{margin: '20px'}}   
  onClick={hook}> SWITCH VIEW </button>
);

class App extends Component {
  constructor(e){
    super(e);
    this.state = {ready: false};
    this.getData();
  }
  async getData(){
    let data = await(await fetch("https://api.travauxlib.com/api/devis-pro/JKusHl8Ba8MABIjdCtLZOe2lxxnUfX")).json();
    let parser = new Devis(data)
    this.setState({ready: true, 
      view: 0,
      // HERE we should not let the parsed data in dict as the STATE
      // but as the regular class variable like this.dict = parser.dict
      //
      // But, for deving purpose and time considerations, and because
      // in real situation I should have a much more clean view of what I
      // need to display, I let it like that.
      ...parser.dict});
  }
  render(){
    const switchView = () => {
      this.setState(s => ({
        view: (s.view + 1) % 2
      }))
    };
    return (
      <div className="App">
        {!this.state.ready ? 
            <div className="lds-dual-ring"/>
            :
            <Fragment>
              <Line>
                <Logo src={this.state.company.logoUrl} />
                <DevisID {...this.state.devis}/>
              </Line>
              <Line className="">
                <Company {...this.state.company}/>
                <Client {...this.state.client}/>
              </Line>
              <Table view={this.state.view} lots={this.state.views[this.state.view]}
              />
              <SwitchButton hook={switchView}/>
              <Line>
                <Payement conditions={this.state.modalitesPaiement}/>
                <Total {...this.state.total} />
              </Line>
              <LineCenter>
                <Signature />
                <Signature client/>
              </LineCenter>
            </Fragment>
        }
      </div>
    );
  }
}

export default App;
