import styled from 'styled-components';
import React, {Fragment} from 'react';

export const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const LineCenter = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const Logo = styled.img`
  width: 250;
  height: 250px;
`;

export const Rounded= styled.div`
  p{
    margin: 0;
    padding: 0;
    ${props => props.notCentered ? `text-align: justify` : ``}
  }
  border: 1px solid;
  padding: 10px;
  border-radius: 20%;
`

export const Client = ({name, mail, address, postalCode, city}) => (
  <Rounded
  >
    <h6>Adresse du Client</h6>
    <p>{name}</p>
    <p>{address}</p>
    <p>{postalCode}</p>
    <p>{city}</p>
  </Rounded>
);
export const Signature = ({client}) => (
  <Rounded 
    notCentered
    style={{
      width: '40%',
      margin: '30px', padding: '20px 10px 110px'
    }}>
    {client ?
        <Fragment>
          <h6>Pour le client</h6>
          {/*
      <LineCenter>
            <p>A</p>
            <p>Le</p>
          </LineCenter>
          <p>Mention</p>
          <LineCenter>
            <p>Nom</p>
            <p>Signature</p>
          </LineCenter>
          */}
        </Fragment>
        :
        <h6>Pour l'entreprise</h6>
    }
  </Rounded>
);
export const Total = (total) => (
  <Rounded 
    notCentered 
    style={{fontWeight: 'bold', padding: '20px', width: '35%'}}>
    {
      Object.entries(total).map(([key, value]) => (
        <Line key={"total" + key}>
          <p>{key} :</p>
          <p> {value}€</p>
        </Line>
      ))
    }
  </Rounded>
);
export const Payement = ({conditions}) => (
  <div>
    <h6>conditions de reglements</h6>
    {
      conditions.map(({label, pourcentage, montant}, i) => (
        <p key={"modalites" + i}>
          {label} de {pourcentage}% <span className="bold">{montant}€</span>
        </p>
      ))
    }
  </div>
);

const Headtd = styled.td`
  font-weight:bold;
  `;      
const Lot = ({view, label, lignes}) => (
  <Fragment>
    <tr>
      <td style={{fontSize:'large', textDecoration:'underline'}} colSpan="6">
        {label}
      </td>
    </tr>
    {
      lignes.map(ligne => (
        <tr key={ligne.designation + view}>
          <td> {ligne.designation} </td>
          <td> {ligne.quantite} </td>
          <td> {ligne.prixUnitaireHT} </td>
          <td> {ligne.prixHT} </td>
          <td> {ligne.montantTVA} </td>
          <td> {ligne.prixTTC} </td>
        </tr>
      ))
    }
  </Fragment>
);
export const Table = ({view, lots}) => (
  <div
    style={{
      marginTop: '20px',
      height: '300px', overflowY:'scroll'
    }}>
  <table>
    <thead>
      <tr >
        <Headtd>Designation</Headtd>
        <Headtd>Quantite</Headtd>
        <Headtd>Prix unitaire</Headtd>
        <Headtd>Total HT</Headtd>
        <Headtd>TVA</Headtd>
        <Headtd>Total TTC</Headtd>
      </tr>
    </thead>
    <tbody>
      {
        lots.map(lot => (
          <Lot view={lot.label} key={lot.label} {...lot}/>
        ))
      }
    </tbody>
  </table>
</div>
)
export const DevisID = ({duree, id, date, availability}) => (
  <div> 
    <p style={{fontWeight:'bold', textDecoration:'underline'}}> DEVIS: </p>
    <p> Date: {date}</p>
    {/*<p> Code Client: </p>*/}
    <p> devis valable {duree} </p>
  </div>
);
export const Company = ({address, capital, city, email, firstNameRepresentantLegal, formattedSiret, insurances, lastNameRepresentantLegal, logoUrl, name, phoneNumber, numeroTVA, postalCode, siren, siret, statusEntreprise, websites}) => (
  <Rounded>
    <p> {name} </p>
    <p> M. {firstNameRepresentantLegal} {lastNameRepresentantLegal} </p>
    <p>{address}</p><p> {capital} {city}</p>
    <p>Tel: {phoneNumber}</p>
    <p>Mail: {email}</p>
  </Rounded>
);
