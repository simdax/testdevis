class Devis{
  constructor(json){
    this.dict = json 
    this.dict.client = {
      name: json.deal.customerName,
      mail: json.deal.customerEmail,
      address: json.deal.billingAddress.address,
      postalCode: json.deal.billingAddress.postalCode,
      city: json.deal.billingAddress.city,
    };
    this.dict.total = this.total(json);
    this.dict.devis = {
      id: json.token,
      date: json.date,
      duree: json.dureeValidite,
    }
    let lots = json.sections[0].lots;
    let lotsByRoom = this.byRoom(lots, json.locations);
    this.dict.views = [lots, lotsByRoom];
  }
  total({prixTotalAvantRemise, montantRemise, prixTotalHT, prixTotalTTC, prixTotalOptionHT, prixTotalFreeHT}){
    return {montantRemise, prixTotalFreeHT, prixTotalHT, prixTotalTTC};
  }
  byRoom(lots, locations){
    let result = [
      ...locations.map(({label, uuid}) => ({uuid, label, lignes: []})),
      {label: "Autres Prestations", lignes: []},
    ];
    lots.forEach(lot => {
      lot.lignes.forEach(ligne => {
        if (!ligne.locationsDetails.quantityIsByLocation)
          result[result.length - 1].lignes.push(ligne)
        else {
          ligne.locationsDetails.locations.forEach(({uuid}) => {
            let l = {...ligne}
            l.quantite = ligne.quantite
            l.prixHT = Math.round(100 * ligne.quantite * l.prixUnitaireHT) / 100;
            l.montantTVA = Math.round(100 * l.prixHT * l.tauxTVA / 100) / 100; 
            l.prixTTC = Math.round(100* (ligne.prixHT + l.montantTVA)) / 100;
            result.find(({uuid: id}) => id === uuid).lignes.push(l);
          })
        }
      });
    });
    return result;
  }
}

export default Devis;
