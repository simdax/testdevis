# Code dont je suis le plus fier

Ce code constitue le module de jeu d'un moteur de son que j'ecrivais en C,
l'annee derniere, qui devrait ouvrir, si tout se passe bien, une veritable
branche sonore a 42.  Etant donne que j'ai commence ma vie par la musique et
que je me suis dirige vers le code par desir de musique generative, ce code a
bien plus de valeur parce ce qu'il represente le fait de pouvoir faire du son,
que par un quelconque raffinement algorithmique.

Concretement, ces fonctions permette de jouer de la polyphonie avec des tables
d'ondes jouees a differentes frequences. Il suffit d'additionner chaque sample
de chacune des voix de la polyphonie. Etonnament, la ligne permettant de
normaliser le signal final apres addition, entre les lignes 125 a 130 est
commente, ce qui laisse a penser que l'application saturera si le nombre de
voix est trop eleve.


```c
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   play.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 09:32:13 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/26 09:49:11 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "sine.h"

void	init_tables(t_app *app)
{
	int		i;
	double	phase;

	i = 0;
	while (i < TABLE_SIZE)
	{
		phase = (double)i / TABLE_SIZE;
		app->sine[i] = sin(phase * M_PI * 2);
		app->sine_plus[i] =
			(sin(phase * M_PI * 2) * 0.5) + (sin(phase * M_PI * 4) * 0.5) +
			(sin(phase * M_PI * 12) * 0.3) + (sin(phase * M_PI * 7) * 0.2) +
			(sin(phase * M_PI * 6) * 0.08) + (sin(phase * M_PI * 9) * 0.02);
		app->triangle[i] = phase < 0.5 ? (phase * 4 - 1) : (phase * -4 + 3);
		app->saw[i] = (phase * 2) + (phase < 0.5 ? 0 : -2);
		app->square[i] = phase < 0.5 ? 1 : -1;
		app->perc[i] = phase < 0.5 ? (phase * 2) : (phase * -2 + 2);
		++i;
	}
}

float	interpolate(double *sine, double index)
{
	float	value1;
	float	value2;
	float	frac;

	frac = index - (int)index;
	value1 = sine[(int)index];
	value2 = sine[(int)index + 1];
	return (value1 + frac * (value2 - value1));
}

void	note_fill_buffer(t_voice *voice, t_note *note, double ddur, double pos)
{
	double	phase;
	double	dur;
	double	index;
	int		i;

	i = BUFFER_SIZE * pos;
	dur = BUFFER_SIZE * ddur;
	phase = note->frequency / BASE_FREQ;
	index = voice->ptr_lut;
	while (i < dur)
	{
		voice->buffer[i] += interpolate(voice->lut, index) * note->volume;
		//* interpolate(voice->env,
		//(1 - note->cycles / note->total_cycles) * TABLE_SIZE);
		index += phase;
		if (index >= TABLE_SIZE)
			index -= TABLE_SIZE;
		++i;
	}
	voice->ptr_lut = index;
}

void	voice_fill_buffer(void *el, void *data, t_array *a)
{
	t_voice *voice;
	t_note	*note;
	double	cycle;
	double	cycle_curr_note;

	voice = el;
	if (voice_check_finish(voice))
		return ;
	cycle = 1;
	while (cycle && voice->notes)
	{
		note = voice->notes->content;
		cycle_curr_note = note->cycles;
		if (cycle_curr_note > cycle)
			cycle_curr_note = cycle;
		note_fill_buffer(voice, note, cycle_curr_note, 1 - cycle);
		cycle -= cycle_curr_note;
		note->cycles -= cycle_curr_note;
		if (!note->cycles)
		{
			voice->notes = voice->notes->next;
			voice_check_finish(voice);
		}
	}
}

int		play_buffer(PaStream *stream, float *buffer)
{
	float	stereo_out[BUFFER_SIZE][2];
	float	max;
	float	min;
	int		i;

	max = 0;
	min = 0;
	i = 0;
	while (i < BUFFER_SIZE)
	{
		if (buffer[i] > max)
			max = buffer[i];
		else if (buffer[i] < min)
			min = buffer[i];
		++i;
	}
	i = -1;
	while (++i < BUFFER_SIZE)
	{
		stereo_out[i][1] = buffer[i];
		stereo_out[i][0] = stereo_out[i][1];
		/*
		 ** printf("min max %f %f\n", min, max);
		 ** stereo_out[i][0] = stereo_out[i][1] =
		 ** (((buffer[i] - min) / (max - min)) - 0.5 );
		 ** printf("%f\n", (stereo_out[i][0]));
		*/
	}
	return (Pa_WriteStream(stream, stereo_out, BUFFER_SIZE));

```

# Code dont je suis moins fier

Pour revenir au theme du developpement web, j'ai choisi un code ecrit en React pour un application d'analyse de match de tennis (le tennis aura une certaine importance semantique).

Les anciennes personnes avec qui je travaillais etaient des mathematiciens qui
ecrivaient du code comme des mathematiciens, c'est-a-dire avec beaucoup de
repetitions et de lourdeurs, et globalement peu de scrupules a l'idee d'etre lu
dans le futur par d'autres personnes.

Avec un autre 'vrai' developpeur de l'incubateur, nous faisions tacitement un
concours a qui ecrira le code le plus court possible pour implementer une
feature.
Le code suivant genere un petit arbre de bouttons representant des moments d'un
match de tennis. Chaque bouton envoyait a un time code d'un lecteur video, et
le tout etait hierarchise ainsi :

```
MOMENTS CLEFS                           -   ...SETS
TYPE DE MOMENT CLEF                     -   ...GAMES
                                        -   ...POINTS
```

Cliquer sur une branche de l'arbre (MOMENTS CLEFS/SETS/GAMES/POINTS) ouvre le
niveau en-dessous de lui (et les feuilles changeait le moment de la video). 

L'idee etait de n'utiliser aucune variable d'etat (qui garderait trace dans le composent des differents 'onglets' ouvert) pour etre totalement 'Reacty', rendant le code plus pur et clairement moins lisible. 

Le component renvoie un array de buttons, qui contiennent chacun une fonction ouvrant son fils, decrit dans la hierarchie d'objects nested dans le SetInfo (ici, par exemple, l'evidente difficulte du terme 'SetInfo', ou le 'set' est polysemique dans le contexte du tennis et de la programmation, est typique d'un code ecrit par mes mathematiciens :) )


```javascript
/* eslint-disable indent */

/*
 * A big reduce function to create a graph of
 * similar buttons nested according to the description
 * inside 'SetInfo'. Pretty elegant, not quite
 * natural !
 */

import React from 'react';
import { ClearButton, FlexColumnCenter } from '~/styles';
import {
  Section,
  SelectedBar,
  OptionsContainer,
} from './styles';

const Options = ({
  parser: { player, setInfo },
  setSelection,
  keys,
  t,
}) => (
  [...keys, false]
  .reduce(({ infos, result }, key, index) => {
    if (infos) {
      result.push(
        <OptionsContainer
          key={`${Object.keys(infos)}`}
        >
          {Object.keys(infos).map(type => (
            <ClearButton
              onClick={() => {
                if (typeof infos[type] === 'number') {
                  player.currentTime(infos[type]);
                } else {
                  setSelection(index, type);
               }
              }}
              key={`${key}:${type}`}
            >
              <FlexColumnCenter>
                <Section> {t(type.toUpperCase())} </Section>
                <SelectedBar hidden={key !== type} />
              </FlexColumnCenter>
            </ClearButton>
          ))}
        </OptionsContainer>);
      return { result, infos: infos[key] };
    }
    return { result };
  }, { result: [], infos: setInfo })
  .result
);

export default Options;
```
